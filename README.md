# Procesamiento del Lenguaje Natural - PLN
![Banner](utils/69566NLP-image.jpeg)
## CEIA Co14
## Alumno:
- Ferrán, Natanael Emir

Este repositorio contiene todos los desafíos de la materia. En cada directorio se encuentra la notebook correspondiente junto a los artefactos y su respectivo README con el contenido detallado del trabajo realizado.
