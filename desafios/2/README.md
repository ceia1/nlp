# Contenido
En este desafío, se entrenan embeddings utilizando como dataset el libro _Animal Farm_ de _George Orwell_.
A partir de los embeddings entrenados, se llevan adelante los siguientes trabajos:
- Análisis de los vectores más similares a algunos ejemplos del vocabulario utilizado.
- Análisis de los vectores menos similares a algunos ejemplos del vocabulario utilizado.
- Tests de analogías.
- Visualizaciones 2D y 3D de los embeddings a partir de una reducción de dimensionalidad con `t-SNE`.
