import requests
import time

from bs4 import BeautifulSoup


def scrap_text(url: str) -> str:
    '''
    ''' 
    max_retries = 3

    for attempt in range(max_retries):
        
        try:
            response = requests.get(url)

            if response.status_code == 200:
                soup = BeautifulSoup(response.content, 'html.parser')
                text = soup.find('td', bgcolor="white").get_text(separator='\n')
                break

            else:
                text = None

        except requests.ConnectionError:
            time.sleep(5) 

    return text