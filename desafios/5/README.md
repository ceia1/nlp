# Contenido
En este desafío, se aprovecha la capacidad de la arquitectura `BERT` para realizar encoding de textos, a fin de construir un modelo de análisis de sentimientos. Para ello, se utiliza un dataset con críticas de _Google Apps_, a fin de predecir la valoración en cada caso.
Se testean distintas arquitecturas y distintos tipos de salida. Finalmente se discuten los resultados obtenidos.
