# Contenido
En este desafío, se entrenan modelos de lenguaje utilizando como dataset el libro _Animal Farm_ de _George Orwell_.
Estos modelos se entrenan por palabra y por caracter en notebooks separadas.

Ambos trabajos consisten en las siguientes actividades:
- Preprocesamiento del dataset.
- Comparación de 2 modelos con arquitecturas distintas.
- Evaluación del mejor modelo mediante distintas inferencias:
    - Predicción del siguiente término.
    - Predicción de los siguientes _n_ términos.
    - Generación de textos mediante _beam search_ determinístico y estocástico.